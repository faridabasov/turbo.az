package az.task.turbo.services;

import az.task.turbo.dto.CarDto;
import az.task.turbo.entity.CarEnt;
import org.springframework.stereotype.Service;

import java.util.List;

public interface CarService {
    public void addNewCar(CarDto dto);
    public void updateCar(Long id, CarDto dto);
    public CarDto getCarByid(Long id);
    public List<CarDto> getCars();
    public void deleteCar(Long id);
    public List<CarDto> searchCar1(CarDto dto);
    public List<CarDto> searchCar2(CarDto dto);
}
