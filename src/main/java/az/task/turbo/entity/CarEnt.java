package az.task.turbo.entity;

import az.task.turbo.enums.ColorType;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "car_ent")
public class CarEnt {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String make;
    private String model;
    private String year;
    private Double price;
    private String fuelType;
    private Integer mileage;
    private Integer engineCapacity;
    private String color;
    private String vin;
}
