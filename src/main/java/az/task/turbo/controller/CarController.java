package az.task.turbo.controller;

import az.task.turbo.dto.CarDto;
import az.task.turbo.services.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/car")
@RequiredArgsConstructor
public class CarController {

    private final CarService service;

    @PostMapping
    void car(@Valid @RequestBody CarDto dto) {
        service.addNewCar(dto);
    }

    @PutMapping("/{id}")
    void car(@PathVariable("id") Long id, @Valid @RequestBody CarDto dto) {
        service.updateCar(id, dto);
    }

    @GetMapping("/{id}")
    CarDto car(@PathVariable("id") Long id) {
        return service.getCarByid(id);
    }

    @GetMapping
    List<CarDto> cars() {
        return service.getCars();
    }

    @DeleteMapping("/{id}")
    void carD(@PathVariable("id") Long id) {
        service.deleteCar(id);
    }

    @PostMapping("/searchWithSpec")
    List<CarDto> cars_1(@RequestBody CarDto dto) {
        return service.searchCar1(dto);
    }

    @PostMapping("/search")
    List<CarDto> car_2(@RequestBody CarDto dto) {
        return service.searchCar2(dto);
    }
}
