package az.task.turbo.enums;

public enum ColorType {
    WHITE, BLACK, RED, GREEN, GREY, YELLOW, BLUE
}
