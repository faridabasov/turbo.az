package az.task.turbo.repository;

import az.task.turbo.entity.CarEnt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<CarEnt, Long> {
}
