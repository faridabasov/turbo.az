package az.task.turbo.serviceImpl;

import az.task.turbo.dto.CarDto;
import az.task.turbo.entity.CarEnt;
import az.task.turbo.entity.CarEnt_;
import az.task.turbo.enums.ColorType;
import az.task.turbo.repository.CarRepository;
import az.task.turbo.services.CarService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {

    private final CarRepository repository;
    private final ModelMapper mapper;
    private final EntityManager em;

    @Override
    public void addNewCar(CarDto dto) {
        CarEnt ent = mapper.map(dto, CarEnt.class);
        repository.save(ent);
    }

    @Override
    public void updateCar(Long id, CarDto dto) {
        CarEnt ent = mapper.map(dto, CarEnt.class);
        ent.setId(id);
        repository.save(ent);
    }

    @Override
    public CarDto getCarByid(Long id) {
        return mapper.map(repository.getById(id), CarDto.class);
    }

    @Override
    public List<CarDto> getCars() {
        return mapper.map(repository.findAll(), new TypeToken<List<CarDto>>() {
        }.getType());
    }

    @Override
    public void deleteCar(Long id) {
        repository.deleteById(id);
    }

    @Override
    public List<CarDto> searchCar1(CarDto dto) {
        if (dto == null)
            return null;
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<CarEnt> query = cb.createQuery(CarEnt.class);
        Root<CarEnt> root = query.from(CarEnt.class);
        List<Predicate> p = new ArrayList<>();

        if (dto.getMake() != null)
            p.add(cb.equal(root.get(CarEnt_.MAKE), dto.getMake()));
        if (dto.getModel() != null)
            p.add(cb.equal(root.get(CarEnt_.MODEL), dto.getModel()));
        if (dto.getModel() != null)
            p.add(cb.equal(root.get(CarEnt_.COLOR), dto.getColor()));
        TypedQuery<CarEnt> tquery = em.createQuery(query);

        return mapper.map(tquery.getResultList(), new TypeToken<List<CarDto>>() {
        }.getType());
    }

    @Override
    public List<CarDto> searchCar2(CarDto dto) {
        CarEnt ent = mapper.map(dto, CarEnt.class);
        List<CarEnt> entList = repository.findAll(Example.of(ent));
        List<CarDto> dtoList = entList.stream().map(x -> mapper.map(x, CarDto.class)).collect(Collectors.toList());
       return dtoList;
    }
}
