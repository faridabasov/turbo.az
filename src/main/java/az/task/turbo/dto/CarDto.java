package az.task.turbo.dto;

import az.task.turbo.enums.ColorType;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class CarDto {
    @NotBlank
    private String make;
    @NotBlank
    private String model;
    @JsonSerialize
    private LocalDate year;
    @NotNull
    private Double price;
    private String fuelType;
    private Integer mileage;
    private Integer engineCapaCity;
    private ColorType color;

    private String vin;
}
